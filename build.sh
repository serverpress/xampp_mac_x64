#!/bin/bash
#
# Build the XAMPP runtime folder for the given version number
#
if [ ! -d "./temp" ]; then 
  mkdir temp
fi

# Ensure no existing xampp
if [ -d "/Applications/XAMPP" ]; then
    echo .
    echo WARNING - /Application/XAMPP folder already exists. Please press Ctrl + c to exit.
    read -p "$*"
else
    curl -L https://www.apachefriends.org/xampp-files/7.4.28/xampp-osx-7.4.28-0-installer.dmg --output temp/xampp-setup.dmg
    hdiutil attach ./temp/xampp-setup.dmg
    sudo /Volumes/XAMPP/xampp-osx-7.4.28-0-installer.app/Contents/MacOS/osx-x86_64 --mode unattended
    hdiutil detach /Volumes/XAMPP

    # Bootstrap XAMPP's php
    export DS_XAMPPFILES="/Applications/XAMPP"
    export DYLD_FALLBACK_LIBRARY_PATH="$DS_XAMPPFILES/lib":$DYLD_FALLBACK_LIBRARY_PATH
    export PATH="$DS_XAMPPFILES/bin":"$DS_XAMPPFILES/sbin":$PATH
    export PHPRC="$DS_XAMPPFILES/etc"
    export PATH="/usr/local/opt/icu4c/bin:$PATH"
    export PATH="/usr/local/opt/icu4c/sbin:$PATH"
    export LDFLAGS="-L/usr/local/opt/icu4c/70.1/lib"
    export CPPFLAGS="-I/usr/local/opt/icu4c/include"
    export PKG_CONFIG_PATH="/usr/local/opt/icu4c/lib/pkgconfig"

    # Build intl.so by downloading php 7.4.8 from scratch per suggestion at
    # https://stackoverflow.com/questions/39012363/install-php-7-0-internationalization-extension-intl-on-xampp-on-mac
    curl -L https://www.php.net/distributions/php-7.4.28.tar.gz --output ./php-7.4.28.tar.gz
    tar -xzvf php-7.4.28.tar.gz
    cd php-7.4.28/ext/intl
    phpize
    ./configure --enable-intl --with-php-config=/Applications/XAMPP/bin/php-config --with-icu-dir=/Applications/XAMPP/xamppfiles/
    make
    sudo make install
    cd ../../../
    rm -rf php-7.4.28
    rm php-7.4.28.tar.gz

    # Build xdebug from scratch
    rm -rf ./xdebug
    git clone https://github.com/xdebug/xdebug.git
    cd xdebug
    phpize
    ./configure --enable-xdebug
    make
    sudo make install
    cd ..
    sudo mv /Applications/XAMPP ./7.4.28
    sudo chmod -R 777 ./7.4.28/xamppfiles/var
    cp ./xdebug/LICENSE ./7.4.28/xamppfiles/licenses/xdebug.txt
fi

# Purge unnecessary files and folders
sudo rm -rf ./7.4.28/uninstall.app
sudo rm -rf ./7.4.28/manager-osx.app
sudo rm -rf ./7.4.28/xamppfiles/manager-osx.app
sudo rm -rf ./7.4.28/xamppfiles/phpmyadmin
sudo rm -rf ./7.4.28/xamppfiles/proftpd
sudo rm -rf ./7.4.28/xamppfiles/build
sudo rm -rf ./7.4.28/xamppfiles/docs
sudo rm -rf ./7.4.28/xamppfiles/include
sudo rm -rf ./7.4.28/xamppfiles/info
sudo rm -rf ./7.4.28/xamppfiles/man
sudo rm -rf ./7.4.28/xamppfiles/manual
sudo rm -rf ./7.4.28/xamppfiles/pear
sudo rm -rf ./7.4.28/xamppfiles/temp
sudo rm -rf ./7.4.28/xamppfiles/var/log
sudo rm -rf ./7.4.28/xamppfiles/var/perl
sudo rm -rf ./7.4.28/xamppfiles/var/run
