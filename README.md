To build the latest runtime

1. Ensure latest xcode is installed (needed to build xdebug)
2. Ensure autoconf is installed (via brew install autoconf)
3. Ensure icu4c installed (via brew install icu4c)
4. Start a command line prompt (Terminal.app)
5. Change directory to project (i.e. /Users/jsmith/Documents/xampp_mac_x64)
6. Execute ./build.sh (be sure to answer any sudo prompts for password)